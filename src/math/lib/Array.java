package math.lib;

/**
 * @author А. С. Крюков
 */
public class Array extends Math {

    /**
     * @param array = Массив целочисленных чисел
     * @return Сумма всех элементов массива
     */
    public int sumOfArrayElements(int[] array) {
        int sum = 0;

        for(int a = 0; a < array.length; a++) {
            sum += array[a];
        }
        return sum;
    }

    /**
     * @param array = Массив дробных чисел
     * @return Сумма всех элементов массива
     */
    public double sumOfArrayElements(double[] array) {
        double sum = 0;

        for(int a = 0; a <array.length; a++) {
            sum += array[a];
        }
        return  sum;
    }

    /**
     * @param array = Массив целочисленных чисел
     * @return Максимальный элемент массива
     */
    public int maxElementOfArray(int[] array) {
        int max = array[0];

        for(int a = 0; a < array.length; a++) {
            if(max < array[a]) max = array[a];
        }
        return max;
    }

    /**
     * @param array Массив дробных чисел
     * @return Максимальный элемент массива
     */
    public double maxElementOfArray(double[] array) {
        double max = array[0];

        for(int a = 0; a < array.length; a++) {
            if (max < array[a]) max = array[a];
        }
        return max;
    }

    /**
     * @param array = Массив целочисленных чисел
     * @return Минимальный элемент массива
     */
    public int minElementOfArray(int[] array) {
        int min = array[0];

        for(int a = 0; a < array.length; a++) {
            if (min > array[a]) min = array[a];
        }
        return min;
    }

    /**
     * @param array = Массив дробных чисел
     * @return Минимальный элемент массива
     */
    public double minElementOfArray(double[] array) {
        double min = array[0];

        for(int a = 0; a < array.length; a++) {
            if (min > array[a]) min = array[a];
        }
        return min;
    }
}
