package math.lib;
/**
 * @author А. С. Крюков
 */
public class Math {

    public final double PI = 3.1415926535898; //Число ПИ
    /**
     * @param a первое число
     * @param b второе число
     * @return c (сумма a и b)
     */
    public int sum(int a, int b) {
        int c = a + b;
        return c;
    }

    /**
     * @param a первое число
     * @param b второе число
     * @return c (сумма a и b)
     */
    public double sum(double a, double b) {
        double c = a + b;
        return  c;
    }

    /**
     * @param a первое число
     * @param b второе число
     * @return c (разница a и b)
     */
    public int subtract(int a, int b) {
        int c = a - b;
        return c;
    }

    /**
     * @param a первое число
     * @param b второе число
     * @return c (разница a и b)
     */
    public double subtract(double a, double b) {
        double c = a - b;
        return c;
    }

    /**
     * @param a первое число
     * @param b второе число
     * @return c (произведение a и b)
     */
    public int multiply(int a, int b) {
        int c = a * b;
        return c;
    }

    /**
     * @param a первое число
     * @param b второе число
     * @return c (произведение a и b)
     */
    public double multiply(double a, double b) {
        double c = a * b;
        return c;
    }

    /**
     * @param a первое число
     * @param b второе число
     * @return c (частное a и b)
     */
    public int divide(int a, int b) {
        int c = a / b;
        return c;
    }

    /**
     * @param a первое число
     * @param b второе число
     * @return c (частное a и b)
     */
    public double divide(double a, double b) {
        double c = a - b;
        return c;
    }

    /**
     * @param a Основание степени
     * @param b Показатель степени
     * @return c Число, возведённое в степень
     */
    public int degree(int a, int b) {
        int c = a;

        for(int d = 0; d < (b - 1); d++) {
            c *= a;
        }
        //c -= a;
        return c;
    }

    /**
     * @param a Основание степени
     * @param b Показатель степени
     * @return c Число, возведённое в степень
     */
    public double degree(double a, double b) {
        double c = a;

        for(double d = 0; d < (b - 1); d++) {
            c *= a;
        }
        return c;
    }

    /**
     * @param a Первый аргумент
     * @param b Второй аргумент
     * @return Наибольший из аргументов
     */
    public int max(int a, int b) {
        if(a > b) {
            return a;
        } else {
            return b;
        }
    }
    /**
     * @param a Первый аргумент
     * @param b Второй аргумент
     * @return Наибольший из аргументов
     */
    public double max(double a, double b) {
        if(a > b) {
            return a;
        } else {
            return b;
        }
    }
    /**
     * @param a Первый аргумент
     * @param b Второй аргумент
     * @return Наибольший из аргументов
     */
    public long max(long a, long b) {
        if(a > b) {
            return a;
        } else {
            return b;
        }
    }
    /**
     * @param a Первый аргумент
     * @param b Второй аргумент
     * @return Наибольший из аргументов
     */
    public float max(float a, float b) {
        if(a > b) {
            return a;
        } else {
            return b;
        }
    }
    /**
     * @param a Первый аргумент
     * @param b Второй аргумент
     * @return Наименьший из аргументов
     */
    public int min(int a, int b) {
        if(a < b) {
            return a;
        } else {
            return b;
        }
    }
    /**
     * @param a Первый аргумент
     * @param b Второй аргумент
     * @return Наименьший из аргументов
     */
    public double min(double a, double b) {
        if(a < b) {
            return a;
        } else {
            return b;
        }
    }
    /**
     * @param a Первый аргумент
     * @param b Второй аргумент
     * @return Наименьший из аргументов
     */
    public long min(long a, long b) {
        if(a < b) {
            return a;
        } else {
            return b;
        }
    }
    /**
     * @param a Первый аргумент
     * @param b Второй аргумент
     * @return Наименьший из аргументов
     */
    public float min(float a, float b) {
        if(a < b) {
            return a;
        } else {
            return b;
        }
    }
    /**
     * @param a Аргумент
     * @return Абсолютное значение (модуль) аргумента
     */
    public int abs(int a) {
        int b = 0;
        if (a > 0) {
            b = a;
        }
        if (a < 0) {
            b = -(a);
        }
        return b;
    }
    /**
     * @param a Аргумент
     * @return Абсолютное значение (модуль) аргумента
     */
    public double abs(double a) {
        double b = 0;
        if (a > 0) {
            b = a;
        }
        if (a < 0) {
            b = -(a);
        }
        return b;
    }
    /**
     * @param a Аргумент
     * @return Абсолютное значение (модуль) аргумента
     */
    public long abs(long a) {
        long b = 0;
        if (a > 0) {
            b = a;
        }
        if (a < 0) {
            b = -(a);
        }
        return b;
    }
    /**
     * @param a Аргумент
     * @return Абсолютное значение (модуль) аргумента
     */
    public float abs(float a) {
        float b = 0;
        if (a > 0) {
            b = a;
        }
        if (a < 0) {
            b = -(a);
        }
        return b;
    }
}
